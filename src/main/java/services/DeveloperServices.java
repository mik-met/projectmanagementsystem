package services;


import dao.DAO;
import dao.DAOSkillImpl;
import models.Developer;
import models.Skill;

import java.util.List;
import java.util.Scanner;

public class DeveloperServices implements Service {
    private static DeveloperServices developerServices;
    private DAO<Developer> developerDAO;

    private DeveloperServices(){}

    public static DeveloperServices getInstance(DAO<Developer> developerDAO) {
        if (developerServices == null){
            developerServices = new DeveloperServices();
        }
        developerServices.developerDAO = developerDAO;
        return developerServices;
    }

    @Override
    public void getAll() {
        try{
            List<Developer> developers = developerDAO.getAll();
            for (Developer developer : developers) {
                System.out.println(developer);
            }
        } catch (RuntimeException e) {
            System.out.println("Output all developers error");
        }
    }

    @Override
    public void create() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Input name, surname, salary using space delimiter");
            String[] developerInput = sc.nextLine().split(" ");
            Developer developer = new Developer(developerInput[0],
                    developerInput[1],
                    Integer.parseInt(developerInput[2]));
            developerDAO.create(developer);
            developer.setId(developerDAO.getIdOfLastInsert());
            System.out.println("Developer\n" + developer.toString() + "\n successfully created.");
        } catch (RuntimeException e) {
            System.out.println("Error creating developer");
        }
    }

    @Override
    public void read() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Input developer id");
            String[] developerInput = sc.nextLine().split(" ");
            int i = Integer.parseInt(developerInput[0]);
            Developer developer = developerDAO.read(i);
            System.out.println(developer.toString());
        } catch (RuntimeException e) {
            System.out.println("Error reading developer");
        }
    }

    @Override
    public void update() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Input name, surname, salary using space delimiter");
            String[] developerInput = sc.nextLine().split(" ");
            Developer developer = new Developer(developerInput[0],
                    developerInput[1],
                    Integer.parseInt(developerInput[2]));
            developerDAO.update(developer);
        } catch (RuntimeException e) {
            System.out.println("Error creating developer");
        }
    }

    @Override
    public void delete() {
        Scanner sc = new Scanner(System.in);
        System.out.println(developerDAO.getAll());
        System.out.println("Input developer's ID which you want to delete");
        String[] str = sc.nextLine().split(" ");
        long id = Long.parseLong(str[0]);
        if (developerDAO.read(id) != null) {
            developerDAO.delete(id);
            System.out.println("Developer with id " + id + " has been deleted");
        } else System.out.println("No such developer.");
    }

    public void changeName() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println(developerDAO.getAll());
            System.out.println("Input developer's ID which you want to change");
            String[] str = sc.nextLine().split(" ");
            int id = Integer.parseInt(str[0]);
            Developer developer = developerDAO.read(id);
            System.out.println("Input name and surname using space delimiter");
            String[] developerInput = sc.nextLine().split(" ");
            developer.setName(developerInput[0]);
            developer.setSurname(developerInput[1]);
            developerDAO.update(developer);
        } catch (RuntimeException e) {
            System.out.println("Error creating developer");
        }
    }

    public void changeSalary() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println(developerDAO.getAll());
            System.out.println("Input developer's ID for whom you want to change salary");
            String[] str = sc.nextLine().split(" ");
            int id = Integer.parseInt(str[0]);
            Developer developer = developerDAO.read(id);
            System.out.println("Input salary value");
            String[] developerInput = sc.nextLine().split(" ");
            developer.setSalary(Integer.parseInt(developerInput[0]));
            developerDAO.update(developer);
        } catch (RuntimeException e) {
            System.out.println("Error creating developer");
        }
    }

    public void assignSkills() {
        try {
            Scanner sc = new Scanner(System.in);
            Developer developer = selectDeveloper();
            System.out.println(DAOSkillImpl.getInstance().getAll());
            System.out.println("Input skill IDs using space delimiter");
            String[] skillsIds = sc.nextLine().split(" ");
            for (int i = 0; i < skillsIds.length; i++) {
                try{
                    long skillId = Integer.parseInt(skillsIds[i]);
                    Skill skill = DAOSkillImpl.getInstance().read(skillId);
                    developer.getSkills().add(skill);
                    System.out.println("Skill " + skill.getName() + " added.");
                } catch (RuntimeException e){
                    System.out.println("Problems with skill " + skillsIds[i] + " . ");
                }
            }
            developerDAO.update(developer);
            System.out.println("New developer data:\n" + developer);
        } catch (RuntimeException e) {
            System.out.println("Error with skills of developer");
        }
    }

    public void removeSkills() {
        try {
            Scanner sc = new Scanner(System.in);
            Developer developer = selectDeveloper();
            System.out.println(developer.getSkills());
            System.out.println("Input skill IDs using space delimiter");
            String[] skillsIds = sc.nextLine().split(" ");
            for (int i = 0; i < skillsIds.length; i++) {
                try{
                    long skillId = Integer.parseInt(skillsIds[i]);
                    Skill skill = DAOSkillImpl.getInstance().read(skillId);
                    developer.getSkills().remove(skill);
                    System.out.println("Skill " + skill.getName() + " removed.");
                } catch (RuntimeException e){
                    System.out.println("Problems with skill " + skillsIds[i] + " . ");
                }
            }
            developerDAO.update(developer);
            System.out.println("New developer data:\n" + developer);
        } catch (RuntimeException e) {
            System.out.println("Error with skills of developer");
        }
    }

    private Developer selectDeveloper(){
        Scanner sc = new Scanner(System.in);
        System.out.println(developerDAO.getAll());
        System.out.println("Input developer's ID for whom you want to change or assign skills");
        String[] str = sc.nextLine().split(" ");
        int id = Integer.parseInt(str[0]);
        Developer developer = developerDAO.read(id);
        return developer;
    }
}
