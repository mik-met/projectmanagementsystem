package dao;

import models.*;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;


public class DAODeveloperImpl extends AbstractDAO<Developer>{
    private static DAODeveloperImpl daoDeveloper;
    private final String SQL_INSERT_UNIQUE_SKILL_OF_DEVELOPER = "insert into devskills (devid, skillid)\n" +
            "select tmp.did, tmp.sid from (select ? as did, ? as sid) as tmp\n" +
            "where not exists (\n" +
            "select devid, skillid from devskills where devid = ? and skillid=?\n" +
            ") limit 1;";

    public static DAODeveloperImpl getInstance() {
        if (daoDeveloper == null) {return daoDeveloper = new DAODeveloperImpl();}
        else return daoDeveloper;
    }

    private DAODeveloperImpl() {
        tableName = "developers";
        SQL_UPDATE_ENTITY = "UPDATE developers SET name = ?, surname = ?, salary = ? WHERE id = ?;";
        SQL_INSERT_ENTITY = "insert into developers (name, surname, salary) values(? , ?, ?);";
    }

    @Override
    public Developer read(long id) {
        Developer developer = super.read(id);
        if (developer == null) return null;
        developer.setSkills(getSkillsOfDeveloper(id));
        return developer;
    }

    @Override
    public Developer update(Developer entity) {
        saveEntitySetIntermediateTable(entity.getSkills(), SQL_INSERT_UNIQUE_SKILL_OF_DEVELOPER, entity.getId(),
                "devskills", "devid");
        return super.update(entity);
    }

    @Override
    protected Developer getEntityFromResultSet(ResultSet rs) throws SQLException {
        Developer developer = new Developer();

        developer.setId(rs.getLong("id"));
        developer.setName(rs.getString("name"));
        developer.setSurname(rs.getString("surname"));
        developer.setSalary(rs.getInt("salary"));
        developer.setSkills(getSkillsOfDeveloper(developer.getId()));
        return developer;
    }

    private Set<Skill> getSkillsOfDeveloper(long id) {
        String SQLgetSkills = "select skillid as id, skillname, devid from skills as s inner join devskills as ds \n" +
                "on s.id = ds.skillid where ds.devid = ?;";
        Set<Skill> skillSet = new HashSet<>();

        try (Connection c = getConnection();
             PreparedStatement psSkills = c.prepareStatement(SQLgetSkills)) {
            psSkills.setLong(1, id);
            ResultSet rsSkills = psSkills.executeQuery();
            while (rsSkills.next()) {
                Skill skill = new Skill();
                skill.setId(rsSkills.getInt("id"));
                skill.setName(rsSkills.getString("skillname"));
                skillSet.add(skill);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return skillSet;
    }

    @Override
    protected void prepareStatementSetEntityParams(PreparedStatement ps, Developer entity) throws SQLException {
        ps.setString(1,entity.getName());
        ps.setString(2,entity.getSurname());
        ps.setInt(3,entity.getSalary());
        if(ps.getParameterMetaData().getParameterCount() > 3) {
            ps.setLong(4, entity.getId());
        }
    }
}

