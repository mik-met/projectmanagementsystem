package UI;

import dao.*;
import models.*;
import services.*;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class View {
    private Scanner scanner = new Scanner(System.in);
    private Services services;
    private List<String> tables = Arrays.asList("developers",
            "skills",
            "companies",
            "customers",
            "projects");

    private View(Services services) {
        this.services = services;
    }

    public static void main(String[] args) {
        DAO<Developer> developerDAO = DAODeveloperImpl.getInstance();
        DAO<Skill> skillDAO = DAOSkillImpl.getInstance();
        DAO<Customer> customerDAO = DAOCustomerImpl.getInstance();
        DAO<Company> comapnyDAO = DAOCompanyImpl.getInstance();
        DAO<Project> projectDAO = DAOProjectImpl.getInstance();

        DeveloperServices developerServices = DeveloperServices.getInstance(developerDAO);
        SkillService skillService = SkillService.getInstance(skillDAO);
        CompaniesService companiesService = CompaniesService.getInstance(comapnyDAO);
        CustomerService customerService = CustomerService.getInstance(customerDAO);
        ProjectService projectService = ProjectService.getInstance(projectDAO);

        Services services = new ServicesImpl(developerServices, skillService,
                companiesService, projectService, customerService);

        View view = new View(services);

        while (true) {
            try {
                view.listTables();
                int i = Integer.parseInt(view.getScanner().next());
                if (i < 0 || i > view.tables.size()) {
                    throw new RuntimeException("Wrong number");
                }
                if (i==0) {break;}
                if (i==1) {view.menuDevelopers();}
                if (i==2) {view.menuSkills();}
                if (i==3) {view.menuCompanies();}
                if (i==4) {view.menuCustomers();}
                if (i==5) {view.menuProjects();}
            } catch (RuntimeException e) {
                System.err.println("Menu mistake");
            }
        }
        view.getScanner().close();
    }

    private void menuProjects() {
        while (true) {
            int i;
            System.out.println("Projects' menu: \n" +
                    "0. exit, \n" +
                    "1. show all,\n" +
                    "2. create,\n" +
                    "3. find by ID,\n" +
                    "4. add developers to Project, \n" +
                    "5. remove developers from Project, \n" +
                    "6. change cost, \n" +
                    "7. delete.\n");
            try{i = Integer.parseInt(getScanner().next());
            } catch (RuntimeException e) {
                System.out.println("Wrong number.");
                return;
            }
            switch (i){
                case 0:
                    return;
                case 1:
                    services.getProjectService().getAll();
                    break;
                case 2:
                    services.getProjectService().create();
                    break;
                case 3:
                    services.getProjectService().read();
                    break;
                case 4:
                    services.getProjectService().addDevelopersToProject();
                    break;
                case 5:
                    services.getProjectService().removeDevelopersFromProject();
                    break;
                case 6:
                    services.getProjectService().changeCost();
                    break;
                case 7:
                    services.getProjectService().delete();
                    break;
            }
        }
    }

    private void menuCustomers() {
        while (true) {
            int i;
            System.out.println("Customers menu: \n" +
                    "0. exit, \n" +
                    "1. show all,\n" +
                    "2. create,\n" +
                    "3. find by ID,\n" +
                    "4. update, \n" +
                    "5. delete.\n");
            try{i = Integer.parseInt(getScanner().next());
            } catch (RuntimeException e) {
                System.out.println("Wrong number.");
                return;
            }
            switch (i){
                case 0:
                    return;
                case 1:
                    services.getCustomerService().getAll();
                    break;
                case 2:
                    services.getCustomerService().create();
                    break;
                case 3:
                    services.getCustomerService().read();
                    break;
                case 4:
                    services.getCustomerService().update();
                    break;
                case 5:
                    services.getCustomerService().delete();
                    break;
            }
        }
    }

    private void menuDevelopers(){
        while (true){
            int i;
            System.out.println("Developer's menu:  \n" +
                    "0. exit, \n" +
                    "1. show all,\n" +
                    "2. create,\n" +
                    "3. find by ID,\n" +
                    "4. change name,\n" +
                    "5. change salary,\n" +
                    "6. assign skills,\n" +
                    "7. remove skills,\n" +
                    "8. delete.\n");
            try{i = Integer.parseInt(getScanner().next());
            } catch (RuntimeException e) {
                System.out.println("Wrong number.");
                return;
            }
            switch (i){
                case 0:
                    return;
                case 1:
                    services.getDeveloperService().getAll();
                    break;
                case 2:
                    services.getDeveloperService().create();
                    break;
                case 3:
                    services.getDeveloperService().read();
                    break;
                case 4:
                    services.getDeveloperService().changeName();
                    break;
                case 5:
                    services.getDeveloperService().changeSalary();
                    break;
                case 6:
                    services.getDeveloperService().assignSkills();
                    break;
                case 7:
                    services.getDeveloperService().removeSkills();
                    break;
                case 8:
                    services.getDeveloperService().delete();
                    break;
            }
        }
    }

    private void menuSkills() {
        while (true) {
            int i;
            System.out.println("Skills menu:  \n" +
                    "0. exit, \n" +
                    "1. show all,\n" +
                    "2. create,\n" +
                    "3. find by ID,\n" +
                    "4. change,\n" +
                    "5. delete.\n");
            try{i = Integer.parseInt(getScanner().next());
            } catch (RuntimeException e) {
                System.out.println("Wrong number.");
                return;
            }
            switch (i){
                case 0:
                    return;
                case 1:
                    services.getSkillService().getAll();
                    break;
                case 2:
                    services.getSkillService().create();
                    break;
                case 3:
                    services.getSkillService().read();
                    break;
                case 4:
                    services.getSkillService().update();
                    break;
                case 5:
                    services.getSkillService().delete();
                    break;
            }
        }
    }

    private void menuCompanies() {
        while (true){
            int i;
            System.out.println("Companies menu:  \n" +
                    "0. exit, \n" +
                    "1. show all,\n" +
                    "2. create,\n" +
                    "3. find by ID,\n" +
                    "4. edit,\n" +
                    "5. add developers,\n" +
                    "6. remove developers,\n" +
                    "7. delete.\n");
            try{i = Integer.parseInt(getScanner().next());
            } catch (RuntimeException e) {
                System.out.println("Wrong number.");
                return;
            }
            switch (i){
                case 0:
                    return;
                case 1:
                    services.getCompaniesServices().getAll();
                    break;
                case 2:
                    services.getCompaniesServices().create();
                    break;
                case 3:
                    services.getCompaniesServices().read();
                    break;
                case 4:
                    services.getCompaniesServices().update();
                    break;
                case 5:
                    services.getCompaniesServices().addDevelopers();
                    break;
                case 6:
                    services.getCompaniesServices().deleteDevelopers();
                    break;
                case 7:
                    services.getCompaniesServices().delete();
                    break;
            }
        }
    }

    private void listTables() {
        System.out.println("0 exit");
        for (int i = 0; i < tables.size(); i++) {
            System.out.println((i + 1) + " " + tables.get(i));
        }
    }

    private Scanner getScanner() {
        return scanner;
    }
}
