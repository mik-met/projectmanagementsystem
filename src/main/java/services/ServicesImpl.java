package services;


public class ServicesImpl implements Services {
    private DeveloperServices developerServices;
    private SkillService skillService;
    private CompaniesService companiesService;
    private ProjectService projectService;
    private CustomerService customerService;

    public ServicesImpl(DeveloperServices developerServices,
                        SkillService skillService,
                        CompaniesService companiesService,
                        ProjectService projectService,
                        CustomerService customerService) {
        this.developerServices = developerServices;
        this.skillService = skillService;
        this.companiesService = companiesService;
        this.projectService = projectService;
        this.customerService = customerService;
    }

    @Override
    public DeveloperServices getDeveloperService() {
        return developerServices;
    }

    @Override
    public SkillService getSkillService() {
        return skillService;
    }

    @Override
    public CustomerService getCustomerService() {
        return customerService;
    }

    @Override
    public ProjectService getProjectService() {
        return projectService;
    }

    @Override
    public CompaniesService getCompaniesServices() {
        return companiesService;
    }
}
