package services;


public interface Services {
    DeveloperServices getDeveloperService();
    SkillService getSkillService();
    CustomerService getCustomerService();
    ProjectService getProjectService();
    CompaniesService getCompaniesServices();
}
