package models;

import java.util.*;

public class Developer implements Entity{
    private long id;
    private int salary;
    private String name;
    private String surname;

    private Set<Skill> skills = new HashSet<>(0);
    //private Set<Company> companies = new HashSet<>(0);
    //private Set<Project> projects = new HashSet<>(0);

    public Developer() {
    }

    public Developer(String name, String surname, int salary) {
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Developer{" +
                "id=" + id +
                ", salary=" + salary +
                ", name='" + name + '\'' +
                ", surname='" + surname +"\n");
        stringBuilder.append("\tskills: ");
        skills.stream().map((Skill s)-> s.getName() + "; ").forEach(stringBuilder::append);
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Developer developer = (Developer) o;

        if (id != developer.id) return false;
        if (salary != developer.salary) return false;
        if (name != null ? !name.equals(developer.name) : developer.name != null) return false;
        if (surname != null ? !surname.equals(developer.surname) : developer.surname != null) return false;
        return skills != null ? skills.equals(developer.skills) : developer.skills == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + salary;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (skills != null ? skills.hashCode() : 0);
        return result;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Set<Skill> getSkills() {
        return skills;
    }

    public void setSkills(Set<Skill> skills) {
        this.skills = skills;
    }
}
