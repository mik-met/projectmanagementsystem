package services;


import dao.DAO;
import models.Customer;

import java.util.Scanner;

public class CustomerService implements Service {
    private static CustomerService customerService;
    private DAO<Customer> customerDAO;

    private CustomerService(){}

    public static CustomerService getInstance(DAO<Customer> customerDAO) {
        if (customerService == null) {
            customerService = new CustomerService();
        }
        customerService.customerDAO = customerDAO;
        return customerService;
    }

    @Override
    public void getAll() {
        System.out.println("List of all customers:");
        System.out.println(customerDAO.getAll());
    }

    @Override
    public void create() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Input customer name ");
            String customerName = sc.nextLine();
            Customer customer = new Customer();
            customer.setName(customerName);
            customerDAO.create(customer);
            System.out.println("Customer " + customer.toString() + "successfully added");
        } catch (RuntimeException e) {
            System.out.println("Error reading customer");
        }
    }

    @Override
    public void read() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Input customer id");
            String[] developerInput = sc.nextLine().split(" ");
            int i = Integer.parseInt(developerInput[0]);
            Customer customer = customerDAO.read(i);
            System.out.println(customer.toString());
        } catch (RuntimeException e) {
            System.out.println("Error reading customer");
        }

    }

    @Override
    public void update() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println(customerDAO.getAll());
            System.out.println("Input customer ID which you want to change");
            String customerString = sc.nextLine();
            int id = Integer.parseInt(customerString);
            Customer customer = customerDAO.read(id);
            System.out.println("Input new customer name");
            customerString = sc.nextLine();
            customer.setName(customerString);
            customerDAO.update(customer);
            System.out.println("Customer " + customer.toString() + "successfully updated");
        } catch (RuntimeException e) {
            System.out.println("Error reading customer");
        }
    }

    @Override
    public void delete() {
        Scanner sc = new Scanner(System.in);
        System.out.println(customerDAO.getAll());
        System.out.println("Input customer ID which you want to delete");
        String[] str = sc.nextLine().split(" ");
        long id = Long.parseLong(str[0]);
        if (customerDAO.read(id) != null) {
            customerDAO.delete(id);
            System.out.println("Customer with id " + id + " has been deleted");
        } else System.out.println("No such customer");
    }
}
