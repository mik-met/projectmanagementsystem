package dao;

import models.Company;
import models.Developer;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;


public class DAOCompanyImpl extends AbstractDAO<Company> {
    private final String SQL_INSERT_UNIQUE_DEVELOPERS_OF_COMPANIES = "insert into devcompanies (comanyid, devid)\n" +
            "select tmp.cid, tmp.did from (select ? as cid, ? as did) as tmp\n" +
            "where not exists (\n" +
            "select comanyid, devid from devcompanies where comanyid = ? and devid = ?\n" +
            ") limit 1;";

    private static DAOCompanyImpl daoCompany;

    public static DAOCompanyImpl getInstance() {
        if (daoCompany == null) {return daoCompany = new DAOCompanyImpl();}
        else return daoCompany;
    }

    private DAOCompanyImpl() {
        tableName = "companies";
        SQL_UPDATE_ENTITY = "update companies set companyname = ? where id = ?;";
        SQL_INSERT_ENTITY = "insert into companies (companyname) values(?);";
    }

    @Override
    public Company read(long id) {
        Company company = super.read(id);
        if (company == null){ return  null;}
        company.setDevelopers(getDevelopersOfCompany(company.getId()));
        return company;
    }

    private Set<Developer> getDevelopersOfCompany(long id) {
        String SQLGetDevelopers = "SELECT * FROM devcompanies where comanyid = " +id+ ";";
        Set<Developer> developers = new HashSet<>(0);
        try (Connection c = getConnection();
             Statement st = c.createStatement()){
            ResultSet rs = st.executeQuery(SQLGetDevelopers);
            while (rs.next()) {
                Developer developer = DAODeveloperImpl.getInstance().read(rs.getLong("devid"));
                if (developer != null){
                    developers.add(developer);
                } else System.out.println("Developer not found!");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
                 e.printStackTrace();
        }
        return developers;
    }

    @Override
    public Company update(Company entity) {
        saveEntitySetIntermediateTable(entity.getDevelopers(), SQL_INSERT_UNIQUE_DEVELOPERS_OF_COMPANIES, entity.getId(),
                "devcompanies", "comanyid");
        return super.update(entity);
    }

    @Override
    protected Company getEntityFromResultSet(ResultSet rs) throws SQLException {
        Company company = new Company();
        company.setId(rs.getInt("id"));
        company.setName(rs.getString("companyname"));
        company.setDevelopers(getDevelopersOfCompany(company.getId()));
        return company;
    }

    @Override
    protected void prepareStatementSetEntityParams(PreparedStatement ps, Company entity) throws SQLException {
        ps.setString(1, entity.getName());
        if(ps.getParameterMetaData().getParameterCount() > 1) {
            ps.setLong(2, entity.getId());}
    }
}
