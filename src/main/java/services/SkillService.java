package services;


import dao.DAO;
import models.Skill;

import java.util.Scanner;

public class SkillService implements Service {
    private static SkillService skillService;
    private DAO<Skill> skillDAO;

    private SkillService(){}

    public static SkillService getInstance(DAO<Skill> skillDAO) {
        if (skillService == null) {
                skillService = new SkillService();
        }
        skillService.skillDAO = skillDAO;
        return skillService;
    }

    @Override
    public void getAll() {
        System.out.println("List of all skills:");
        System.out.println(skillDAO.getAll());
    }

    @Override
    public void create() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Input skill name ");
            String[] skillName = sc.nextLine().split(" ");
            Skill skill = new Skill();
            skill.setName(skillName[0]);
            skillDAO.create(skill);
        } catch (RuntimeException e) {
            System.out.println("Error creating skill");
        }
    }

    @Override
    public void read() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Input skill id");
            String[] str = sc.nextLine().split(" ");
            int i = Integer.parseInt(str[0]);
            Skill skill = skillDAO.read(i);
            System.out.println(skill.toString());
        } catch (RuntimeException e) {
            System.out.println("Error reading skill");
        }
    }

    @Override
    public void update() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println(skillDAO.getAll());
            System.out.println("Input skill ID which you want to change");
            String[] skillString = sc.nextLine().split(" ");
            int id = Integer.parseInt(skillString[0]);
            Skill skill = skillDAO.read(id);
            System.out.println("Input new skill name");
            skillString = sc.nextLine().split(" ");
            skill.setName(skillString[0]);
            skillDAO.update(skill);
            System.out.println("Skill" + skill.toString() + "successfully updated");
        } catch (RuntimeException e) {
            System.out.println("Error updating skill");
        }
    }

    @Override
    public void delete() {
        Scanner sc = new Scanner(System.in);
        System.out.println(skillDAO.getAll());
        System.out.println("Input skill ID which you want to delete");
        String[] skillString = sc.nextLine().split(" ");
        long id = Long.parseLong(skillString[0]);
        if (skillDAO.read(id) != null) {
            skillDAO.delete(id);
            System.out.println("Skill with id " + id + " has been deleted");
        } else System.out.println("No such skill");
    }
}
