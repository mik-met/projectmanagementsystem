package dao;

import models.Skill;
import java.sql.*;

public class DAOSkillImpl extends AbstractDAO<Skill> {
    private static DAOSkillImpl daoSkill;

    public static DAOSkillImpl getInstance() {
        if (daoSkill == null) {return daoSkill = new DAOSkillImpl();}
        else return daoSkill;
    }

    private DAOSkillImpl() {
        tableName = "skills";
        SQL_UPDATE_ENTITY = "update skills set skillname = ? where id = ?;";
        SQL_INSERT_ENTITY = "insert into skills (skillname) values(?);";
    }

    @Override
    protected Skill getEntityFromResultSet(ResultSet rs) throws SQLException {
        Skill skill = new Skill();
        skill.setId(rs.getInt("id"));
        skill.setName(rs.getString("skillname"));
        return skill;
    }

    @Override
    protected void prepareStatementSetEntityParams(PreparedStatement ps, Skill entity) throws SQLException {
        ps.setString(1, entity.getName());
        if(ps.getParameterMetaData().getParameterCount() > 1) {
            ps.setLong(2, entity.getId());}
    }
}
