package dao;

import models.Customer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class DAOCustomerImpl extends AbstractDAO<Customer> {
    private static DAOCustomerImpl daoCustomer;

    public static DAOCustomerImpl getInstance() {
        if (daoCustomer == null) {return daoCustomer = new DAOCustomerImpl();}
        else return daoCustomer;
    }

    private DAOCustomerImpl() {
        tableName = "customers";
        SQL_UPDATE_ENTITY = "UPDATE customers SET customername = ? WHERE id = ?;";
        SQL_INSERT_ENTITY = "insert into customers (customername) values(?);";
    }

    @Override
    protected Customer getEntityFromResultSet(ResultSet rs) throws SQLException {
        Customer customer = new Customer();
        customer.setId(rs.getInt("id"));
        customer.setName(rs.getString("customername"));
        return customer;
    }

    @Override
    protected void prepareStatementSetEntityParams(PreparedStatement ps, Customer entity) throws SQLException {
        ps.setString(1, entity.getName());
        if(ps.getParameterMetaData().getParameterCount() > 1) {
            ps.setLong(2, entity.getId());
        }
    }
}
