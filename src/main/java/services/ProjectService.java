package services;


import dao.*;
import models.Developer;
import models.Project;

import java.util.Scanner;

public class ProjectService implements Service {
    private static ProjectService projectService;
    private DAO<Project> projectDAO;

    private ProjectService(){}

    public static ProjectService getInstance(DAO<Project> projectDAO) {
        if (projectService == null) {
            projectService = new ProjectService();
        }
        projectService.projectDAO = projectDAO;
        return projectService;
    }

    @Override
    public void getAll() {
        System.out.println("List of all projects:");
        System.out.println(projectDAO.getAll());
    }

    @Override
    public void create() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Input project name ");
            String projectName = sc.nextLine();
            Project project = new Project();
            project.setName(projectName);
            System.out.println(DAOCustomerImpl.getInstance().getAll());
            System.out.println("select customer id");
            long customerId = Long.parseLong(sc.nextLine().split(" ")[0]);
            System.out.println("input cost:");
            int cost = Integer.parseInt(sc.nextLine().split(" ")[0]);
            project.setCustomer(DAOCustomerImpl.getInstance().read(customerId));
            project.setCost(cost);
            projectDAO.create(project);
            if (projectDAO.read(project.getId()) != null) {
                System.out.println("Project " + project.toString() + " added");
            } else System.out.println("Project wasn't created!");
        } catch (RuntimeException e) {
            System.out.println("Error creating project");
        }
    }

    @Override
    public void read() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Input project id");
            String[] developerInput = sc.nextLine().split(" ");
            int i = Integer.parseInt(developerInput[0]);
            Project project = projectDAO.read(i);
            System.out.println(project.toString());
        } catch (RuntimeException e) {
            System.out.println("Error reading project");
        }
    }

    @Override
    public void update() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println(projectDAO.getAll());
            System.out.println("Input project ID which you want to change");
            String[] str = sc.nextLine().split(" ");
            int id = Integer.parseInt(str[0]);
            Project  project = projectDAO.read(id);
            System.out.println("Input new project name");
            str = sc.nextLine().split(" ");
            project.setName(str[0]);
            projectDAO.update(project);
            System.out.println("Project " + project.toString() + "successfully updated");
        } catch (RuntimeException e) {
            System.out.println("Error reading project");
        }
    }

    @Override
    public void delete() {
        Scanner sc = new Scanner(System.in);
        System.out.println(projectDAO.getAll());
        System.out.println("Input project ID which you want to delete");
        String[] str = sc.nextLine().split(" ");
        long id = Long.parseLong(str[0]);
        if (projectDAO.read(id) != null){
            projectDAO.delete(id);
            System.out.println("Project with id " + id + " has been deleted");
        } else System.out.println("Project not found.");
    }

    public void addDevelopersToProject() {
        Scanner sc = new Scanner(System.in);
        Project project = selectProject();
        if (project == null) {
            System.out.println("Project not found");
            return;
        }
        System.out.println(DAODeveloperImpl.getInstance().getAll());
        System.out.println("Input developers' id IDs whom you want to add to Project");
        String[] devIds = sc.nextLine().split(" ");
        for (int i = 0; i < devIds.length; i++) {
            try{
                long devLongId = Long.parseLong(devIds[i]);
                Developer developer = DAODeveloperImpl.getInstance().read(devLongId);
                if (developer != null) {
                    project.getDevelopers().add(developer);
                } else {
                    System.out.println("Developer with id " + devLongId + " not found");
                }
            } catch (RuntimeException e) {
                System.out.println();
            }
        }
        projectDAO.update(project);
        System.out.println("Project " + project + " updated");
    }

    public void removeDevelopersFromProject() {
        Scanner sc = new Scanner(System.in);
        Project project = selectProject();
        if (project == null) {
            System.out.println("Project not found");
            return;
        }
        System.out.println(project.getDevelopers());
        System.out.println("Input developers' id IDs whom you want to remove from Project");
        String[] devIds = sc.nextLine().split(" ");
        for (int i = 0; i < devIds.length; i++) {
            try{
                long devLongId = Long.parseLong(devIds[i]);
                Developer developer = DAODeveloperImpl.getInstance().read(devLongId);
                if (developer != null) {
                    project.getDevelopers().remove(developer);
                } else {
                    System.out.println("Developer with id " + devLongId + " not found");
                }
            } catch (RuntimeException e) {
                System.out.println();
            }
        }
        projectDAO.update(project);
        System.out.println("Project " + projectDAO.read(project.getId()) + " updated");

    }

    public void changeCost() {
        try {
            Scanner sc = new Scanner(System.in);
            Project project = selectProject();
            System.out.println("Input cost value");
            String[] costString = sc.nextLine().split(" ");
            project.setCost(Integer.parseInt(costString[0]));
            projectDAO.update(project);
        } catch (RuntimeException e) {
            System.out.println("Error changing project data");
        }
    }

    private Project selectProject() {
        Scanner sc = new Scanner(System.in);
        System.out.println(projectDAO.getAll());
        System.out.println("Input Project ID: ");
        String[] str = sc.nextLine().split(" ");
        long id = Long.parseLong(str[0]);
        Project project = projectDAO.read(id);
        return project;
    }
}
