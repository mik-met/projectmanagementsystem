package models;

import java.util.HashSet;
import java.util.Set;

public class Project implements Entity {
    private long id;
    private int cost;
    private String name;
    private Customer customer;
    private Set<Developer> developers = new HashSet<>(0);

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Project{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", cost=" + cost + "\n" +
                "\t, customer = " + customer.toString() +
                "\t, developrs = \n");
        for (Developer developer : developers) {
            stringBuilder.append("\t\t" + developer.getId() + " " + developer.getName() + " " + developer.getSurname() + "\n");
        }
        return stringBuilder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Project project = (Project) o;

        if (id != project.id) return false;
        if (cost != project.cost) return false;
        if (name != null ? !name.equals(project.name) : project.name != null) return false;
        if (customer != null ? !customer.equals(project.customer) : project.customer != null) return false;
        return developers != null ? developers.equals(project.developers) : project.developers == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + cost;
        result = 31 * result + (customer != null ? customer.hashCode() : 0);
        result = 31 * result + (developers != null ? developers.hashCode() : 0);
        return result;
    }

    public Set<Developer> getDevelopers() {
        return developers;
    }

    public void setDevelopers(Set<Developer> developers) {
        this.developers = developers;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}
