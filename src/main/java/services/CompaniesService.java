package services;


import dao.DAO;
import dao.DAODeveloperImpl;
import models.Company;
import models.Developer;

import java.util.Scanner;

public class CompaniesService implements Service {
    private static CompaniesService companiesService;
    private DAO<Company> companyDAO;

    private CompaniesService(){}

    public static CompaniesService getInstance(DAO<Company> companyDAO) {
        if (companiesService == null) {
            companiesService = new CompaniesService();
        }
        companiesService.companyDAO = companyDAO;
        return companiesService;
    }

    @Override
    public void getAll() {
        System.out.println("List of all companies:");
        System.out.println(companyDAO.getAll());
    }

    @Override
    public void create() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Input company name ");
            String companyName = sc.nextLine();
            Company company = new Company();
            company.setName(companyName);
            companyDAO.create(company);
            System.out.println("Company " + company.toString() + " successfully added");
        } catch (RuntimeException e) {
            System.out.println("Error reading company");
        }
    }

    @Override
    public void read() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Input company id");
            String[] developerInput = sc.nextLine().split(" ");
            int i = Integer.parseInt(developerInput[0]);

            Company company = companyDAO.read(i);
            System.out.println(company.toString());
        } catch (RuntimeException e) {
            System.out.println("Error reading company");
        }
    }

    @Override
    public void update() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println(companyDAO.getAll());
            System.out.println("Input company ID which you want to change");
            String str = sc.nextLine();
            int id = Integer.parseInt(str);
            Company company = companyDAO.read(id);
            System.out.println("Input new company name");
            str = sc.nextLine();
            company.setName(str);
            companyDAO.update(company);
            System.out.println("Company " + company.toString() + "successfully updated");
        } catch (RuntimeException e) {
            System.out.println("Error reading company");
        }
    }

    @Override
    public void delete() {
        Scanner sc = new Scanner(System.in);
        System.out.println(companyDAO.getAll());
        System.out.println("Input company ID which you want to delete");
        String[] str = sc.nextLine().split(" ");
        long id = Long.parseLong(str[0]);
        if (companyDAO.read(id) != null) {
            companyDAO.delete(id);
            System.out.println("Company with id " + id + " has been deleted");
        } else System.out.println("No such companie.");
    }

    public void addDevelopers() {
        Scanner sc = new Scanner(System.in);
        Company company = selectCompany();
        System.out.println(DAODeveloperImpl.getInstance().getAll());
        System.out.println("Input developers' IDs whom you want to add to company");
        String[] devIds = sc.nextLine().split(" ");
        for (int i = 0; i < devIds.length; i++) {
            try{
                long devLongId = Long.parseLong(devIds[i]);
                Developer developer = DAODeveloperImpl.getInstance().read(devLongId);
                if (developer != null) {
                    company.getDevelopers().add(developer);
                } else {
                    System.out.println("Developer with id " + devLongId + " not found");
                }
            } catch (RuntimeException e) {
                System.out.println();
            }
        }
        companyDAO.update(company);
        System.out.println("Company " + company + " updated");
    }

    public void deleteDevelopers() {
        Scanner sc = new Scanner(System.in);
        Company company = selectCompany();
        System.out.println(company.getDevelopers());
        System.out.println("Input developers' IDs whom you want to remove from company");
        String[] devIds = sc.nextLine().split(" ");
        for (int i = 0; i < devIds.length; i++) {
            try{
                long devLongId = Long.parseLong(devIds[i]);
                Developer developer = DAODeveloperImpl.getInstance().read(devLongId);
                if (developer != null) {
                    company.getDevelopers().remove(developer);
                } else {
                    System.out.println("Developer with id " + devLongId + " not found");
                }
            } catch (RuntimeException e) {
                System.out.println();
            }
        }
        companyDAO.update(company);
        System.out.println("Company " + company + " updated");
    }

    private Company selectCompany(){
        Scanner sc = new Scanner(System.in);
        System.out.println(companyDAO.getAll());
        System.out.println("Input company ID where you want to add developers");
        String[] str = sc.nextLine().split(" ");
        long id = Long.parseLong(str[0]);
        Company company = companyDAO.read(id);
        return company;
    }
}
