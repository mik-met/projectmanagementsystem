package dao;

import java.util.List;

public interface DAO<D> {
    List<D> getAll();
    void create(D d);
    D read(long id);
    D update(D d);
    void delete(long id);
    long getIdOfLastInsert();
}
