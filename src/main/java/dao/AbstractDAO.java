package dao;


import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import models.Entity;

import java.beans.PropertyVetoException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public abstract class AbstractDAO<T extends Entity> implements DAO<T> {
    String tableName;
    String SQL_UPDATE_ENTITY;
    String SQL_INSERT_ENTITY;

    private String getTableName(){
        return tableName;
    }
    private static ComboPooledDataSource dataSource = new ComboPooledDataSource();

    static {
        String url = "jdbc:mysql://localhost:3306/hw1";
        String name = "root";
        String password = "1111";
        try {
            dataSource.setDriverClass("com.mysql.jdbc.Driver");
        } catch (PropertyVetoException e) {
            e.printStackTrace();
        }
        dataSource.setJdbcUrl(url);
        dataSource.setUser(name);
        dataSource.setPassword(password);
        dataSource.setMinPoolSize(3);
    }

    protected Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    protected abstract T getEntityFromResultSet(ResultSet rs) throws SQLException;
    protected abstract void prepareStatementSetEntityParams(PreparedStatement ps, T entity) throws SQLException;

    @Override
    public List<T> getAll() {
        List<T> entities = new ArrayList<>();
        String sqlSelectAll = "select * from " + getTableName() + ";";
        try(Connection c = getConnection();
            PreparedStatement ps = c.prepareStatement(sqlSelectAll)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                T entity = getEntityFromResultSet(rs);
                entities.add(entity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return entities;
    }

    @Override
    public void create(T entity) {
        try(Connection c = getConnection();
            PreparedStatement ps = getCreationPreparedStatement(c, entity) ){
            ps.executeQuery();
        } catch (MySQLIntegrityConstraintViolationException e){
            System.out.println("There already is such entity!");
        }
        catch (SQLException e) {
            System.out.println("Not created");
        }
    }

    private PreparedStatement getCreationPreparedStatement(Connection c, T entity) throws SQLException {
        PreparedStatement ps = c.prepareStatement(SQL_INSERT_ENTITY);
        prepareStatementSetEntityParams(ps, entity);
        return ps;
    }

    @Override
    public T read(long id) {
        try (Connection c = getConnection();
             PreparedStatement ps = c.prepareStatement("select * from " + getTableName() + " where id = ?;")){
            ps.setLong(1,id);
            ResultSet rs = ps.executeQuery();
            rs.next();
            return getEntityFromResultSet(rs);
        } catch (SQLException e) {
            System.out.println("No such entity");
            return null;
        }
    }

    @Override
    public T update(T entity) {
        try( Connection c = getConnection();
             PreparedStatement psGetOld = c.prepareStatement("select * from " + getTableName() + " where id = ?;");
             PreparedStatement psUpdate = c.prepareStatement(SQL_UPDATE_ENTITY)){
            psGetOld.setLong(1, entity.getId());
            ResultSet rs = psGetOld.executeQuery();
            rs.next();
            T oldEntity = getEntityFromResultSet(rs);

            prepareStatementSetEntityParams(psUpdate, entity);
            psUpdate.execute();
            return oldEntity;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void delete(long id) {
        String sql = "DELETE FROM " + getTableName() + " WHERE id = ?;";

        try(Connection c = getConnection();
            PreparedStatement ps = c.prepareStatement(sql)) {
            ps.setLong(1,id);
            ps.execute();
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public long getIdOfLastInsert(){
        String sql = "select distinct LAST_INSERT_ID() as id_last from " + getTableName() + ";";
        try(Connection c = getConnection();
        Statement statement = c.createStatement()) {
            ResultSet rs = statement.executeQuery(sql);
            rs.next();
            System.out.println(rs.getString("id_last"));
            return 0;
            } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("no inserts to database found");
        }
    }

    protected <E extends Entity> void saveEntitySetIntermediateTable(Set<E> set, String sql, long parentId,
            String intermediateTableName, String stringParentIdFieldName) {
        String sqlClear = "DELETE from " + intermediateTableName + " WHERE " + stringParentIdFieldName + " = " +
                parentId + ";";
        Connection c;
        try {
            c = getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
        try (Statement statementRemove = c.createStatement();
             PreparedStatement ps = c.prepareStatement(sql)){
            c.setAutoCommit(false);
            statementRemove.execute(sqlClear);
            for (E e: set) {
                ps.setLong(1, parentId);
                ps.setLong(2, e.getId());
                ps.setLong(3, parentId);
                ps.setLong(4, e.getId());
                ps.addBatch();
            }
            ps.executeBatch();
            c.commit();
            c.setAutoCommit(false);
        } catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            System.out.println("Update error");
        } finally {
            if (c != null) {
                try {
                    c.setAutoCommit(false);
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
