package dao;

import models.Developer;
import models.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;


public class DAOProjectImpl extends AbstractDAO<Project> {
    private static DAOProjectImpl daoProject;
    private final String SQL_INSERT_UNIQUE_DEVELOPERS_OF_PROJECT = "insert into devprojects (projid, devid)\n" +
            "select tmp.pid, tmp.did from (select ? as pid, ? as did) as tmp\n" +
            "where not exists (\n" +
            "select devid, projid from devprojects where projid = ? and devid = ?\n" +
            ") limit 1;";

    private DAOProjectImpl() {
        tableName = "projects";
        SQL_UPDATE_ENTITY = "update projects set projectname = ?, customerid = ?, cost = ? where id = ?;";
        SQL_INSERT_ENTITY = "insert into projects (projectname, customerid, cost) values(?, ?, ?);";
    }

    public static DAOProjectImpl getInstance() {
        if (daoProject == null) {return daoProject = new DAOProjectImpl();}
        else return daoProject;
    }

    @Override
    public void create(Project entity) {
        super.create(entity);
    }

    @Override
    public Project read(long id) {
        Project project = super.read(id);
        if(project == null) {
            System.out.println("No such project");
            return null;
        }
        project.setDevelopers(getDevelopersOfProject(id));
        return project;
    }

    private Set<Developer> getDevelopersOfProject(long id) {
        String SqlGetProjects = "SELECT dp.projid, dp.devid FROM devprojects as dp WHERE dp.projid = ?;";
        Set<Developer> developers = new HashSet<>();

        try (Connection c = getConnection();
             PreparedStatement ps = c.prepareStatement(SqlGetProjects)){
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                long devId = rs.getLong("devid");
                Developer developer = DAODeveloperImpl.getInstance().read(devId);
                if (developer != null){
                    developers.add(developer);
                } else System.out.println("Developer not found for project " + id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return developers;
    }

    @Override
    public Project update(Project entity) {
        saveEntitySetIntermediateTable(entity.getDevelopers(), SQL_INSERT_UNIQUE_DEVELOPERS_OF_PROJECT, entity.getId(),
                "devprojects", "projid");
        return super.update(entity);
    }

    @Override
    protected Project getEntityFromResultSet(ResultSet rs) throws SQLException {
        Project project = new Project();

        project.setId(rs.getInt("id"));
        project.setName(rs.getString("projectname"));
        project.setCost(rs.getInt("cost"));
        int customerId = rs.getInt("customerid");
        project.setCustomer(DAOCustomerImpl.getInstance().read(customerId));
        project.setDevelopers(getDevelopersOfProject(project.getId()));
        return project;
    }

    @Override
    protected void prepareStatementSetEntityParams(PreparedStatement ps, Project entity) throws SQLException {
        ps.setString(1,entity.getName());
        ps.setLong(2,entity.getCustomer().getId());
        ps.setInt(3,entity.getCost());
        if(ps.getParameterMetaData().getParameterCount() > 3) {
            ps.setLong(4, entity.getId());
        }
    }
}
