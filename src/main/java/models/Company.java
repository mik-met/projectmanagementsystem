package models;

import java.util.HashSet;
import java.util.Set;

public class Company implements Entity {
    private long id;
    private String name;
    private Set<Developer> developers = new HashSet<>(0);

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Company{" +
                "id=" + id +
                ", name='" + name + '\'' +
                "\t, developrs = \n");
        for (Developer developer : developers) {
            stringBuilder.append("\t\t" + developer.getId() + " " + developer.getName() + " " + developer.getSurname() + "\n");
        }
        return stringBuilder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Company company = (Company) o;

        if (id != company.id) return false;
        return name != null ? name.equals(company.name) : company.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    public Set<Developer> getDevelopers() {
        return developers;
    }

    public void setDevelopers(Set<Developer> developers) {
        this.developers = developers;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
