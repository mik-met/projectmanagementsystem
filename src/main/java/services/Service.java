package services;


public interface Service {
    void getAll();
    void create();
    void read();
    void update();
    void delete();
}
