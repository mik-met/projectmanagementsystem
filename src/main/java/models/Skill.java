package models;

public class Skill implements Entity {
    private long id;
    private String skillname;

    @Override
    public String toString() {
        return  "id - " + id + ", " +
                "name - " + skillname + "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Skill skill = (Skill) o;

        if (id != skill.id) return false;
        return skillname != null ? skillname.equals(skill.skillname) : skill.skillname == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (skillname != null ? skillname.hashCode() : 0);
        return result;
    }

    public long getId() {
        return id;
    }
    
    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return skillname;
    }

    public void setName(String skillname) {
        this.skillname = skillname;
    }
}
